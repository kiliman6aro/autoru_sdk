<?php


namespace Dealer\Handler;


use Dealer\Model\Session;
use Psr\Http\Message\ResponseInterface;

class LoginHandler implements Handler
{
    /**
     * @param ResponseInterface $response
     * @return Session
     */
    public function handle(ResponseInterface $response)
    {
        $obj = json_decode($response->getBody());
        return new Session($obj->session->id, $obj->session->user_id, $obj->user->profile->autoru->client_id, $obj->session->device_uid, $obj->session->creation_timestamp, $obj->session->expire_timestamp, $obj->session->ttl_sec);
    }

}