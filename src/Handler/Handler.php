<?php


namespace Dealer\Handler;

use Psr\Http\Message\ResponseInterface;

interface Handler
{
    public function handle(ResponseInterface $response);
}