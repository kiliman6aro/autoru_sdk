<?php


namespace Dealer\Api;


use Dealer\Rest\ApiContext;

abstract class Resource
{
    /**
     * @var ApiContext $context
     */
    protected $context;

    /**
     * Resource constructor.
     * @param ApiContext $context
     */
    public function __construct(ApiContext $context)
    {
        $this->context = $context;
    }
}