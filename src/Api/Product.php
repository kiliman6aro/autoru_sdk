<?php


namespace Dealer\Api;


use Dealer\Exception\ServiceValidationException;
use Dealer\Validation\RemoveServiceValidator;
use GuzzleHttp\RequestOptions;
use Dealer\Model\Service;

class Product extends Resource
{
    const CATEGORY_CARS = 'cars';

    const CATEGORY_MOTO = 'moto';

    const CATEGORY_TRUCKS = 'trucks';

    const CATEGORY_ALL = 'all';


    /**
     * Helper method for add one service
     * @param $category
     * @param $offerId
     * @param Service $service
     * @return bool
     *
     */
    public function addService($category, $offerId, Service $service)
    {
        return $this->addServices($category, $offerId, [$service]);
    }

    /**
     * @param $category
     * @param $offerId
     * @param Service[] $services
     * @return bool
     *
     */
    public function addServices($category, $offerId, $services)
    {
        foreach ($services as $service){
            $service->validate();
        }
        $res = $this->context->getClient()->post(sprintf('user/offers/%s/%d/products',$category, $offerId), [
            RequestOptions::JSON => [
                'products' => $services
            ]
        ]);
        return ($res->getStatusCode() === 200);
    }

    /**
     * @param $category
     * @param $offerId
     * @param Service $service
     * @return bool
     * @throws ServiceValidationException
     */
    public function removeService($category, $offerId, $service)
    {
        RemoveServiceValidator::validate($service);
        $res = $this->context->getClient()->delete(sprintf('user/offers/%s/%d/products?product=%s',$category, $offerId, $service->code));
        return ($res->getStatusCode() === 200);
    }

    /**
     * @param $category
     * @param $offerId
     * @param Service[] $services
     * @return bool
     * @throws ServiceValidationException
     */
    public function removeServices($category, $offerId, $services)
    {
        $query = '';
        foreach ($services as $service){
            RemoveServiceValidator::validate($service);
            $query .= 'product='.$service->code.'&';
        }
        rtrim($query, '&');

        $res = $this->context->getClient()->delete(sprintf('user/offers/%s/%d/products?%s',$category, $offerId, $query));
        return ($res->getStatusCode() === 200);
    }
}