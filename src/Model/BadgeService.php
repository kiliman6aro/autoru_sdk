<?php


namespace Dealer\Model;


use Dealer\Validation\BadgeServiceValidator;

class BadgeService extends Service
{

    /**
     * @var array $badges
     */
    public $badges = [];

    /**
     * BadgeService constructor.
     * @param array $badges
     */
    public function __construct($badges)
    {
        $this->badges = $badges;
        parent::__construct();
    }


    public function getCode()
    {
        return 'all_sale_badge';
    }

    /**
     * @return bool
     * @throws \Dealer\Exception\ServiceValidationException
     */
    public function validate()
    {
        return BadgeServiceValidator::validate($this);
    }
}