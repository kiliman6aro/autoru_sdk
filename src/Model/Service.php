<?php


namespace Dealer\Model;


abstract class Service
{

    /**
     * @var string $code
     */
    public $code;

    /**
     * Service constructor.
     */
    public function __construct()
    {
        $this->code = $this->getCode();
    }

    /**
     * @return string code of service
     */
    public abstract function getCode();

    public abstract function validate();



}