<?php


namespace Dealer\Model;


class Session
{
    public $id;

    public $user_id;

    public $client_id;

    public $device_uid;

    public $created_at;

    public $expire_at;

    public $ttl_sec;

    /**
     * Session constructor.
     * @param $id
     * @param $userId
     * @param $clientId
     * @param $deviceUid
     * @param $createdAt
     * @param $expireAt
     * @param $ttlSec
     */
    public function __construct($id, $userId, $clientId, $deviceUid, $createdAt, $expireAt, $ttlSec)
    {
        $this->id = $id;
        $this->user_id = $userId;
        $this->client_id = $clientId;
        $this->device_uid = $deviceUid;
        $this->created_at = $createdAt;
        $this->expire_at = $expireAt;
        $this->ttl_sec = $ttlSec;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getDeviceUid()
    {
        return $this->device_uid;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getExpireAt()
    {
        return $this->expire_at;
    }

    /**
     * @return mixed
     */
    public function getTtlSec()
    {
        return $this->ttl_sec;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @param mixed $clientId
     */
    public function setClientId($clientId)
    {
        $this->client_id = $clientId;
    }



}