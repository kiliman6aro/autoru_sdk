<?php


namespace Dealer\Rest;


use Dealer\Handler\LoginHandler;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Dealer\Model\Session;

class ApiContext
{

    const BASE_URL = 'https://apiauto.ru/1.0/';
    /**
     * @var string $token
     */
    private $token;


    /**
     * @var Client $client
     */
    private $client;

    /**
     * @var string $login
     */
    private $login;

    /**
     * @var string $password
     */
    private $password;

    /**
     * @var Session $session
     */
    private $session;

    /**
     * ApiContext constructor.
     * @param string $token
     * @param string $login
     * @param string $password
     */
    public function __construct($token, $login, $password)
    {
        $this->token = $token;
        $this->client = $this->makeClient();
        $this->login = $login;
        $this->password = $password;

        $res = $this->client->post('auth/login', [
            RequestOptions::JSON => [
                'login' => $this->login,
                'password' => $this->password,
                'options' => [
                    'allow_client_login' => true
                ]
            ]
        ]);
        if($res->getStatusCode() === 200){
            $handler = new LoginHandler();
            $this->session = $handler->handle($res);
            $this->client = $this->makeClient($this->session);
        }
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Session|null $session
     * @return Client
     */
    private function makeClient(Session $session = null)
    {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'X-Authorization' => $this->token
        ];
        if($session){
            $headers['x-dealer-id'] = $session->getClientId();
            $headers['x-session-id'] = $session->getId();
        }

        return new Client([
            'base_uri' => self::BASE_URL,
            'headers' => $headers
        ]);
    }

}