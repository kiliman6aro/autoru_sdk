<?php


namespace Dealer\Validation;


use Dealer\Exception\BadgeServiceValidationException;
use Dealer\Model\BadgeService;

/**
 * Class BadgeServiceValidator
 * @package Dealer\Validation
 */
class BadgeServiceValidator
{

    /**
     * Helper method for validating
     * @return bool
     * @throws BadgeServiceValidationException
     */
    public static function validate(BadgeService $service)
    {
        $count = count($service->badges);
        if($count == 0 || $count > 3){
            throw new BadgeServiceValidationException("Badges must be greater than zero and no more than 3");
        }
        return true;
    }
}