<?php


namespace Dealer\Validation;


use Dealer\Exception\ServiceValidationException;
use Dealer\Model\FreshService;
use Dealer\Model\Service;
use Dealer\Model\TurboService;

class RemoveServiceValidator
{
    /**
     * Helper method for validating remove a service
     * @param Service $service
     * @return bool
     * @throws ServiceValidationException
     */
    public static function validate(Service $service)
    {
        if($service instanceof FreshService || $service instanceof TurboService){
            throw new ServiceValidationException("{$service->getCode()} service cannot be removed.");
        }
        return true;
    }
}